
package view;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Pokemon;
import model.TipoPokemon;

public class VisuTipoAtr extends javax.swing.JFrame {
    //VisuTipo tipo = new VisuTipo();
    
    TipoPokemon tipopok = new TipoPokemon ();
    Pokemon pok = new Pokemon();
    public ArrayList<String> tipo =  new ArrayList();
    public ArrayList<String> pokemon =  new ArrayList();
    String pokSel; 
    
    
    public VisuTipoAtr() {
        initComponents();
        
        this.cbAtributos.removeAllItems();
        
        String csvFile = "DadosTipos.csv";
        String line = "";
        String cvsSplitBy = ",";
        
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {
                
            String[] dados = line.split(cvsSplitBy);
  
            tipopok.nome= dados[0];
            tipo.add(tipopok.nome);
            //System.out.println(dados[0]);
            
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        for(String t: tipo){
            cbAtributos.addItem(t);
        }
            //System.out.println(tipo);
 

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        cbAtributos = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        Visualizar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(64, 178, 231));

        cbAtributos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbAtributos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbAtributosActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel1.setText("Selecione o Pokémon");

        Visualizar.setText("Visualizar");
        Visualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VisualizarActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/skrelp.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Visualizar)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbAtributos, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(jLabel2))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(cbAtributos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Visualizar))
                    .addComponent(jLabel2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(83, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbAtributosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAtributosActionPerformed
        
    }//GEN-LAST:event_cbAtributosActionPerformed

    private void VisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VisualizarActionPerformed
        
        
        
        String csvFile = "pokemon.csv";
        String line = "";
        String cvsSplitBy = ",";
        
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {
                
            String[] dados = line.split(cvsSplitBy);
  
            pok.nome = dados[0];
            pokemon.add(pok.nome);
            
            pok.tipo_1 = dados[1];
            pokemon.add(pok.tipo_1);
            
            pok.tipo_2 = dados[2];
            pokemon.add(pok.tipo_2);
            
            pok.ataque = dados[3];
            pokemon.add(pok.ataque);
            
            pok.defesa = dados[4];
            pokemon.add(pok.defesa);
            
            pok.velocidade = dados[5];
            pokemon.add(pok.velocidade);
            
            pok.geracao = dados[6];
            pokemon.add(pok.geracao);
            
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        pokSel =  (String) cbAtributos.getSelectedItem();
        /**System.out.print(pokSel + "\n");
        System.out.print(pokemon.size() + "\n");*/
       
        for (int i=0; i<pokemon.size(); i+=7){ 
            
            if(pokemon.get(i).equals(pokSel)){   
                //System.err.println("Pokémon achado:  \n" + pokemon.get(i) + "\n");

                    String resu = "Nome: " + pokemon.get(i) + "\n" + "Tipo 1: " + pokemon.get(i+1) + 
                            "\n" + "Tipo 2: " + pokemon.get(i+2) + "\n" + "Ataque: " + pokemon.get(i+3) +
                             "\n" + "Defesa: " + pokemon.get(i+4) + "\n" + "Velocidade: " + pokemon.get(i+5)
                            + "\n" + "Geração: " + pokemon.get(i+6) + "\n";
                    
                    int add = JOptionPane.showConfirmDialog(null,resu,"Deseja adicionar Pokémons? ",JOptionPane.YES_NO_OPTION);
                   
                    if(add == 0){
                        AdicionarPok addi = new AdicionarPok();
                        addi.setVisible(true);
                          
                        //System.out.println(treinador.pokemonsAdd);
                    }else{
                        System.exit(0);
                    }
            }
        } 
    }//GEN-LAST:event_VisualizarActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VisuTipoAtr.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VisuTipoAtr.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VisuTipoAtr.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VisuTipoAtr.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VisuTipoAtr().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Visualizar;
    private javax.swing.JComboBox<String> cbAtributos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
