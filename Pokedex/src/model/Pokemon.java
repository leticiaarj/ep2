package model;



public class Pokemon {
   
    public String nome;
    public String tipo_1;
    public String tipo_2;
    public String ataque;
    public String defesa;
    public String velocidade;
    public String geracao;
    public String retorno;
    
    public Pokemon(){
        
        this.nome = nome;
        this.tipo_1 = tipo_1;
        this.tipo_2 = tipo_2;
        this.ataque = ataque;
        this.defesa = defesa;
        this.velocidade = velocidade;
        this.geracao = geracao;     
    }
    
    public String getNome(){
        return nome;
    }
    public void setNome(String nome){
         this.nome = nome;
    }
    
    
    public String getTipo_1(){
        return tipo_1;
    }
    public void setTipo_1(String tipo_1){
         this.tipo_1 = tipo_1;
    }
    
    
    public String getTipo_2(){
        return tipo_2;
    }
    public void setTipo_2(String tipo_2){
         this.tipo_2 = tipo_2;
    }
    
    
    public String getAtaque(){
        return ataque;
    }
    public void setAtaque(String ataque){
         this.ataque = ataque;
    }
 
    
    public String getDefesa(){
        return defesa;
    }
    public void setDefesa(String defesa){
         this.defesa = defesa;
    }
    
    public String getVelocidade(){
        return velocidade;
    }
    public void setVelocidade(String velocidade){
         this.velocidade = velocidade;
    }
    
    
    public String getGeracao(){
        return geracao;
    }
    public void setGeracao(String geracao){
         this.geracao = geracao;
    }
    
}
