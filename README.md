# Pokedéx

## Introdução

O programa tem como objetivo realizar os procedimentos de uma Pokédex. 

## Objetivo

Construir uma Pokédex simples. Onde os treinadores possam se cadastrar,
e adicionar Pokémons. E possam ver as características destes Pokémons.

## Pré- Requisitor

* O Software foi desenvolvido no NetBeans IDE 8.2;

* Desenvovido no ambiente Windows;

* O software aceita o cadastro de até trës pokémons por treinador;

* Os diagramas só abrem no netbeans


### Funcionalidades da Pokédex

    * Pesquisar Pokémons pelo nome;
    * Pesquisar Pokémons pelo tipo;
    * Cadastrar um treinador;
    * Selecionar um treinador e atribuir os Pokémons que este possui;
    * Visualizar os Pokémons de um treinador.

#### Autoria 

* Nome: Letícia Karla Soares Rodrigues de Araújo;
* Matrícula: 15/0135939.
